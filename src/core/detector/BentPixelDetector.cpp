/** @file
 *  @brief Implementation of the detector model
 *  @copyright Copyright (c) 2017-2020 CERN and the Corryvreckan authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include <fstream>
#include <map>
#include <string>

#include "Math/DisplacementVector2D.h"
#include "Math/PositionVector3D.h"
#include "Math/RotationX.h"
#include "Math/RotationY.h"
#include "Math/RotationZ.h"
#include "Math/RotationZYX.h"

#include "BentPixelDetector.hpp"
#include "core/utils/log.h"
#include "exceptions.h"

using namespace ROOT::Math;
using namespace corryvreckan;

BentPixelDetector::BentPixelDetector(const Configuration& config) : Detector(config) {

    // Set detector position and direction from configuration file
    SetPostionAndOrientation(config);

    // initialize transform
    this->initialise();

    // Auxiliary devices don't have: number_of_pixels, pixel_pitch, spatial_resolution, mask_file, region-of-interest
    if(!isAuxiliary()) {
        build_axes(config);
    }
}

void BentPixelDetector::build_axes(const Configuration& config) {

    m_nPixels = config.get<ROOT::Math::DisplacementVector2D<Cartesian2D<int>>>("number_of_pixels");
    // Size of the pixels:
    m_pitch = config.get<ROOT::Math::XYVector>("pixel_pitch");
    // Geometry related parameters
    m_coordinates = config.get<std::string>("coordinates");
    m_flat_part = config.get<double>("flat_part", 0);
    m_radius = config.get<double>("radius", 0);
    m_bent_axis = config.get<std::string>("bent_axis", "column");
    std::transform(m_bent_axis.begin(), m_bent_axis.end(), m_bent_axis.begin(), ::tolower);
    if(m_bent_axis == "column" || m_bent_axis == "col") {
        m_bent_axis = "column";
    } else if(m_bent_axis == "row") {
        m_bent_axis = "row";
    } else {
        throw InvalidValueError(config, "bent_axis", "Bent axis can only be set to \"column\" or \"row\" for now");
    }

    LOG(TRACE) << "Initialized \"" << m_detectorType << "\": " << m_nPixels.X() << "x" << m_nPixels.Y() << " px, pitch of "
               << Units::display(m_pitch, {"mm", "um"});
    LOG(TRACE) << "Initialized \""
               << "BentPixelDetector"
               << "\": " << m_bent_axis << " is bent axis, "
               << "bending radius of " << Units::display(m_radius, {"mm"}) << ", flat part of "
               << Units::display(m_flat_part, {"mm"});

    if(Units::convert(m_pitch.X(), "mm") >= 1 or Units::convert(m_pitch.Y(), "mm") >= 1 or
       Units::convert(m_pitch.X(), "um") <= 1 or Units::convert(m_pitch.Y(), "um") <= 1) {
        LOG(WARNING) << "Pixel pitch unphysical for detector " << m_detectorName << ": " << std::endl
                     << Units::display(m_pitch, {"nm", "um", "mm"});
    }

    // Intrinsic spatial resolution, defaults to pitch/sqrt(12):
    m_spatial_resolution = config.get<ROOT::Math::XYVector>("spatial_resolution", m_pitch / std::sqrt(12));
    if(!config.has("spatial_resolution")) {
        LOG(WARNING) << "Spatial resolution for detector '" << m_detectorName << "' not set." << std::endl
                     << "Using pitch/sqrt(12) as default";
    }

    // region of interest:
    m_roi = config.getMatrix<int>("roi", std::vector<std::vector<int>>());

    if(config.has("mask_file")) {
        m_maskfile_name = config.get<std::string>("mask_file");
        std::string mask_file = config.getPath("mask_file", true);
        LOG(DEBUG) << "Adding mask to detector \"" << config.getName() << "\", reading from " << mask_file;
        maskFile(mask_file);
        process_mask_file();
    }
}

void BentPixelDetector::SetPostionAndOrientation(const Configuration& config) {
    // Detector position and orientation
    m_displacement = config.get<ROOT::Math::XYZPoint>("position", ROOT::Math::XYZPoint());
    m_orientation = config.get<ROOT::Math::XYZVector>("orientation", ROOT::Math::XYZVector());
    m_orientation_mode = config.get<std::string>("orientation_mode", "xyz");

    if(m_orientation_mode != "xyz" && m_orientation_mode != "zyx" && m_orientation_mode != "zxz") {
        throw InvalidValueError(config, "orientation_mode", "orientation_mode should be either 'zyx', xyz' or 'zxz'");
    }

    LOG(WARNING) << "BentPixelDetector!";
    LOG(TRACE) << "  Position:    " << Units::display(m_displacement, {"mm", "um"});
    LOG(TRACE) << "  Orientation: " << Units::display(m_orientation, {"deg"}) << " (" << m_orientation_mode << ")";
    LOG(TRACE) << "  m_displacement: X=" << m_displacement.X() << " Y=" << m_displacement.Y() << " Z=" << m_displacement.Z();
    LOG(TRACE) << "  m_orientation:  X=" << m_orientation.X() << " Y=" << m_orientation.Y() << " Z=" << m_orientation.Z();
}

void BentPixelDetector::process_mask_file() {
    // Open the file with masked pixels
    std::ifstream inputMaskFile(m_maskfile, std::ios::in);
    if(!inputMaskFile.is_open()) {
        LOG(WARNING) << "Could not open mask file " << m_maskfile;
    } else {
        int row = 0, col = 0;
        std::string id;
        // loop over all lines and apply masks
        while(inputMaskFile >> id) {
            if(id == "c") {
                inputMaskFile >> col;
                if(col > nPixels().X() - 1) {
                    LOG(WARNING) << "Column " << col << " outside of pixel matrix, chip has only " << nPixels().X()
                                 << " columns!";
                }
                LOG(TRACE) << "Masking column " << col;
                for(int r = 0; r < nPixels().Y(); r++) {
                    maskChannel(col, r);
                }
            } else if(id == "r") {
                inputMaskFile >> row;
                if(row > nPixels().Y() - 1) {
                    LOG(WARNING) << "Row " << col << " outside of pixel matrix, chip has only " << nPixels().Y() << " rows!";
                }
                LOG(TRACE) << "Masking row " << row;
                for(int c = 0; c < nPixels().X(); c++) {
                    maskChannel(c, row);
                }
            } else if(id == "p") {
                inputMaskFile >> col >> row;
                if(col > nPixels().X() - 1 || row > nPixels().Y() - 1) {
                    LOG(WARNING) << "Pixel " << col << " " << row << " outside of pixel matrix, chip has only "
                                 << nPixels().X() << " x " << nPixels().Y() << " pixels!";
                }
                LOG(TRACE) << "Masking pixel " << col << " " << row;
                maskChannel(col, row); // Flag to mask a pixel
            } else {
                LOG(WARNING) << "Could not parse mask entry (id \"" << id << "\")";
            }
        }
        LOG(INFO) << m_masked.size() << " masked pixels";
    }
}

void BentPixelDetector::maskChannel(int chX, int chY) {
    int channelID = chX + m_nPixels.X() * chY;
    m_masked[channelID] = true;
}

bool BentPixelDetector::masked(int chX, int chY) const {
    int channelID = chX + m_nPixels.X() * chY;
    if(m_masked.count(channelID) > 0)
        return true;
    return false;
}

// Function to initialise transforms
void BentPixelDetector::initialise() {

    // Make the local to global transform, built from a displacement and
    // rotation
    Translation3D translations = Translation3D(m_displacement.X(), m_displacement.Y(), m_displacement.Z());

    Rotation3D rotations;
    if(m_orientation_mode == "xyz") {
        LOG(DEBUG) << "Interpreting Euler angles as XYZ rotation";
        // First angle given in the configuration file is around x, second around y, last around z:
        rotations = RotationZ(m_orientation.Z()) * RotationY(m_orientation.Y()) * RotationX(m_orientation.X());
    } else if(m_orientation_mode == "zyx") {
        LOG(DEBUG) << "Interpreting Euler angles as ZYX rotation";
        // First angle given in the configuration file is around z, second around y, last around x:
        rotations = RotationZYX(m_orientation.x(), m_orientation.y(), m_orientation.z());
    } else if(m_orientation_mode == "zxz") {
        LOG(DEBUG) << "Interpreting Euler angles as ZXZ rotation";
        // First angle given in the configuration file is around z, second around x, last around z:
        rotations = EulerAngles(m_orientation.x(), m_orientation.y(), m_orientation.z());
    } else {
        throw InvalidSettingError(this, "orientation_mode", "orientation_mode should be either 'zyx', xyz' or 'zxz'");
    }

    m_localToGlobal = Transform3D(rotations, translations);
    m_globalToLocal = m_localToGlobal.Inverse();

    // Find the normal to the detector surface. Build two points, the origin and a unit step in z,
    // transform these points to the global coordinate frame and then make a vector pointing between them
    m_origin = PositionVector3D<Cartesian3D<double>>(0., 0., 0.);
    m_origin = m_localToGlobal * m_origin;
    PositionVector3D<Cartesian3D<double>> localZ(0., 0., 1.);
    localZ = m_localToGlobal * localZ;
    m_normal = PositionVector3D<Cartesian3D<double>>(
        localZ.X() - m_origin.X(), localZ.Y() - m_origin.Y(), localZ.Z() - m_origin.Z());
}

// Only if detector is not auxiliary
void BentPixelDetector::configure_detector(Configuration& config) const {

    // Number of pixels
    config.set("number_of_pixels", m_nPixels);

    // Size of the pixels
    config.set("pixel_pitch", m_pitch, {"um"});

    // Intrinsic resolution:
    config.set("spatial_resolution", m_spatial_resolution, {"um"});

    // Pixel mask file:
    if(!m_maskfile_name.empty()) {
        config.set("mask_file", m_maskfile_name);
    }

    // Region-of-interest:
    config.setMatrix("roi", m_roi);
}

void BentPixelDetector::configure_pos_and_orientation(Configuration& config) const {
    config.set("position", m_displacement, {"um", "mm"});
    config.set("orientation_mode", m_orientation_mode);
    config.set("orientation", m_orientation, {"deg"});
    config.set("coordinates", m_coordinates);
    config.set("radius", m_radius, {"mm"});
    config.set("flat_part", m_flat_part, {"mm"});
    config.set("bent_axis", m_bent_axis);
}

XYZPoint BentPixelDetector::localToGlobal(XYZPoint local) const {
    ROOT::Math::XYVector detector_dimensions = getSize();
    // which axis of the sensor is bent
    if(m_bent_axis == "column") {
        if(m_flat_part != 0 || m_radius == 0) {
            LOG(WARNING) << "Column bending does not allow for a flat part at the moment or zero radius. Return cartesian "
                            "coordinates";
            return m_localToGlobal * local;
        }
        // origin of coordinate system in the middle of the flat chip (tangential point)
        double col_arc_length = local.X();
        ROOT::Math::XYZPoint local_transformed(
            m_radius *
                sin(col_arc_length / m_radius), // >0 for col_arc_length>0 and vice versa independent of sign(m_radius)
            local.y(),
            local.z() - m_radius * (1.0 - cos(col_arc_length / m_radius))); // local.z() + ... for r<0
        local_transformed = m_localToGlobal * local_transformed;
        LOG(TRACE) << "Transformed local point (" << local.x() << "|" << local.y() << "|" << local.z()
                   << ") to global point (" << local_transformed.x() << "|" << local_transformed.y() << "|"
                   << local_transformed.z() << "). Bent column";
        return local_transformed;
    } else if(m_bent_axis == "row") {
        if(m_radius == 0) {
            LOG(WARNING) << "Zero radius. Return cartesian coordinates";
            return m_localToGlobal * local;
        }
        // origin of coordinate system in the middle of the flat chip
        double row_arc_length = detector_dimensions.Y() / 2 - local.Y(); // distance from top of the chip
        if(row_arc_length < m_flat_part) {                               // flat sensor
            ROOT::Math::XYZPoint local_transformed(local.x(), -row_arc_length + detector_dimensions.Y() / 2, local.z());
            local_transformed = m_localToGlobal * local_transformed;
            LOG(TRACE) << "Transformed local point (" << local.x() << "|" << local.y() << "|" << local.z()
                       << ") to global point (" << local_transformed.x() << "|" << local_transformed.y() << "|"
                       << local_transformed.z() << "). Bent row";
            return local_transformed;
        } else { // bent sensor
            ROOT::Math::XYZPoint local_transformed(
                local.x(),
                -m_radius * sin((row_arc_length - m_flat_part) / m_radius) + detector_dimensions.Y() / 2 -
                    m_flat_part,                                                                // works for negative radius
                m_radius * (cos((row_arc_length - m_flat_part) / m_radius) - 1.0) + local.z()); // works for negative radius
            local_transformed = m_localToGlobal * local_transformed;
            LOG(TRACE) << "Transformed local point (" << local.x() << "|" << local.y() << "|" << local.z()
                       << ") to global point (" << local_transformed.x() << "|" << local_transformed.y() << "|"
                       << local_transformed.z() << "). Bent row";
            return local_transformed;
        }
    } else {
        LOG(ERROR) << "m_bent_axis = " << m_bent_axis << " not implemented. Return cartesian coordinates";
        return m_localToGlobal * local;
    }
}

XYZPoint BentPixelDetector::globalToLocal(XYZPoint global) const {
    ROOT::Math::XYVector detector_dimensions = getSize();
    // transform from global to bent local
    ROOT::Math::XYZPoint local_transformed = m_globalToLocal * global;
    // which axis of the sensor is bent
    if(m_bent_axis == "column") {
        if(m_flat_part != 0 || m_radius == 0) {
            LOG(WARNING) << "Column bending does not allow for a flat part at the moment. Return cartesian coordinates";
            return m_globalToLocal * global;
        }
        // origin of coordinate system in the middle of the chip (tangential point)
        double col_arc_length = m_radius * asin(local_transformed.x() / m_radius);
        ROOT::Math::XYZPoint local(col_arc_length, local_transformed.y(), 0);
        LOG(TRACE) << "Transformed global point (" << global.x() << "|" << global.y() << "|" << global.z()
                   << ") to local point (" << local.x() << "|" << local.y() << "|" << local.z() << "). Bent column";
        return local;
    } else if(m_bent_axis == "row") {
        if(m_radius == 0) {
            LOG(WARNING) << "Zero radius. Return cartesian coordinates";
            return m_globalToLocal * global;
        }
        double row_arc_length =
            m_radius * asin((detector_dimensions.Y() / 2 - m_flat_part - local_transformed.Y()) / m_radius) + m_flat_part;
        // origin of coordinate in the middle of the flat chip
        if(row_arc_length < m_flat_part) { // flat part
            ROOT::Math::XYZPoint local(local_transformed.x(), local_transformed.y(), 0);
            LOG(TRACE) << "Transformed global point (" << global.x() << "|" << global.y() << "|" << global.z()
                       << ") to local point (" << local.x() << "|" << local.y() << "|" << local.z() << "). Bent row";
            return local;
        } else {                                              // bent part
            ROOT::Math::XYZPoint local(local_transformed.x(), // not modified
                                       detector_dimensions.Y() / 2 - row_arc_length,
                                       0);
            LOG(TRACE) << "Transformed global point (" << global.x() << "|" << global.y() << "|" << global.z()
                       << ") to local point (" << local.x() << "|" << local.y() << "|" << local.z() << "). Bent row";
            return local;
        }
    } else {
        LOG(ERROR) << "m_bent_axis = " << m_bent_axis << " not implemented. Return cartesian coordinates";
        return m_globalToLocal * global;
    }
}

PositionVector3D<Cartesian3D<double>> BentPixelDetector::getIntercept(const Track* track) const {
    // FIXME: this else statement can only be temporary
    if(track->getType() == "GblTrack") {
        return track->getState(getName());
    } else {
        // Get the distance from the plane to the track initial state
        double distance = (m_origin.X() - track->getState(m_detectorName).X()) * m_normal.X();
        distance += (m_origin.Y() - track->getState(m_detectorName).Y()) * m_normal.Y();
        distance += (m_origin.Z() - track->getState(m_detectorName).Z()) * m_normal.Z();
        distance /= (track->getDirection(m_detectorName).X() * m_normal.X() +
                     track->getDirection(m_detectorName).Y() * m_normal.Y() +
                     track->getDirection(m_detectorName).Z() * m_normal.Z());

        // Propagate the track
        PositionVector3D<Cartesian3D<double>> globalPlanarIntercept(
            track->getState(m_detectorName).X() + distance * track->getDirection(m_detectorName).X(),
            track->getState(m_detectorName).Y() + distance * track->getDirection(m_detectorName).Y(),
            track->getState(m_detectorName).Z() + distance * track->getDirection(m_detectorName).Z());

        // Get and transform track state and direction
        PositionVector3D<Cartesian3D<double>> state_track = track->getState(m_detectorName);
        DisplacementVector3D<Cartesian3D<double>> direction_track = track->getDirection(m_detectorName);

        // Bring track to local (transformed) coordinate system
        state_track = m_globalToLocal * state_track;
        direction_track = m_globalToLocal.Rotation() * direction_track;
        direction_track = direction_track.Unit();

        // From globalPlanarIntercept get intercept with bent surface of pixel detector
        double intercept_parameters[2] = {-999999, -999999};
        if(m_bent_axis == "column") {
            if(m_flat_part != 0 || m_radius == 0) {
                LOG(WARNING) << "Column bending does not allow for a flat part at the moment or zero radius. Return "
                                "globalPlanarIntercept";
                return globalPlanarIntercept;
            }

            // Define/initialise detector cylinder in local_transformed coordinates
            ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<double>> state_cylinder(0, 0, -m_radius);
            ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<double>> direction_cylinder(
                0, 1, 0); // cylinder axis along y (row)

            getInterceptParameters(state_track, direction_track, state_cylinder, direction_cylinder, intercept_parameters);

        } else if(m_bent_axis == "row") {
            if(m_radius == 0) {
                LOG(WARNING) << "Zero radius. Return globalPlanarIntercept";
                return globalPlanarIntercept;
            }

            double row_arc_length =
                m_radius * asin((this->getSize().Y() / 2 - m_flat_part - globalPlanarIntercept.Y()) / m_radius) +
                m_flat_part;
            if(row_arc_length < m_flat_part) { // flat part
                return globalPlanarIntercept;
            } else { // bent part
                ROOT::Math::PositionVector3D<ROOT::Math::Cartesian3D<double>> state_cylinder(
                    0, this->getSize().Y() / 2 - m_flat_part, -m_radius);
                ROOT::Math::DisplacementVector3D<ROOT::Math::Cartesian3D<double>> direction_cylinder(
                    1, 0, 0); // cylinder axis along x (column)

                getInterceptParameters(
                    state_track, direction_track, state_cylinder, direction_cylinder, intercept_parameters);
            }
        } else {
            LOG(ERROR) << "m_bent_axis = " << m_bent_axis << " not implemented. Return globalPlanarIntercept";
            return globalPlanarIntercept;
        }

        // Select solution according to bending direction
        PositionVector3D<Cartesian3D<double>> localBentIntercept;
        if(m_radius <= 0) { // for negative radius select smaller solution
            if(intercept_parameters[0] < -999990) {
                LOG(TRACE) << "No intercept of track and cylindrcal sensor found. Return intercept outside of sensor area";
                return (localToGlobal(getLocalPosition(2 * m_nPixels.X(), 2 * m_nPixels.Y())));
            } else {
                localBentIntercept = ROOT::Math::XYZPoint(state_track.x() + intercept_parameters[0] * direction_track.x(),
                                                          state_track.y() + intercept_parameters[0] * direction_track.y(),
                                                          state_track.z() + intercept_parameters[0] * direction_track.z());
            }
        } else {
            if(intercept_parameters[1] < -999990) {
                LOG(TRACE) << "No intercept of track and cylindrcal sensor found. Return intercept outside of sensor area";
                return (localToGlobal(getLocalPosition(2 * m_nPixels.X(), 2 * m_nPixels.Y())));
            } else {
                localBentIntercept = ROOT::Math::XYZPoint(state_track.x() + intercept_parameters[1] * direction_track.x(),
                                                          state_track.y() + intercept_parameters[1] * direction_track.y(),
                                                          state_track.z() + intercept_parameters[1] * direction_track.z());
            }
        }

        return m_localToGlobal * localBentIntercept;
    } // not GBL track
}

void BentPixelDetector::getInterceptParameters(const PositionVector3D<Cartesian3D<double>>& state_track,
                                               const DisplacementVector3D<Cartesian3D<double>>& direction_track,
                                               const PositionVector3D<Cartesian3D<double>>& state_cylinder,
                                               const DisplacementVector3D<Cartesian3D<double>>& direction_cylinder,
                                               double (&result)[2]) const {
    // Get factors for quadratic equation: alpha z^2 + beta * z + gamma
    double alpha;
    double beta;
    double gamma;
    if(direction_cylinder.X() != 0 && direction_cylinder.Y() == 0 &&
       direction_cylinder.Z() == 0) { // cylinder along x (column)
        alpha = (direction_track.Y() * direction_track.Y()) + (direction_track.Z() * direction_track.Z());
        beta = 2 * (state_track.Y() * direction_track.Y() + state_track.Z() * direction_track.Z() -
                    direction_track.Y() * state_cylinder.Y() - direction_track.Z() * state_cylinder.Z());
        gamma =
            ((state_track.Y() * state_track.Y()) + (state_track.Z() * state_track.Z()) +
             (state_cylinder.Y() * state_cylinder.Y()) + (state_cylinder.Z() * state_cylinder.Z()) -
             2 * state_track.Y() * state_cylinder.Y() - 2 * state_track.Z() * state_cylinder.Z() - (m_radius * m_radius));
    } else if(direction_cylinder.X() == 0 && direction_cylinder.Y() != 0 &&
              direction_cylinder.Z() == 0) { // cylinder along y (row)
        alpha = (direction_track.X() * direction_track.X()) + (direction_track.Z() * direction_track.Z());
        beta = 2 * (state_track.X() * direction_track.X() + state_track.Z() * direction_track.Z() -
                    direction_track.X() * state_cylinder.X() - direction_track.Z() * state_cylinder.Z());
        gamma =
            ((state_track.X() * state_track.X()) + (state_track.Z() * state_track.Z()) +
             (state_cylinder.X() * state_cylinder.X()) + (state_cylinder.Z() * state_cylinder.Z()) -
             2 * state_track.X() * state_cylinder.X() - 2 * state_track.Z() * state_cylinder.Z() - (m_radius * m_radius));
    } else {
        LOG(ERROR) << "Unknown cylinder direction";
        result[0] = -999999;
        result[1] = -999999;
        return;
    }

    // Check if the quadratic equation has a solution
    double radical = beta * beta - 4 * alpha * gamma;
    if(radical < 0 || alpha == 0) { // return a value outside the chip for the track to have no intercept
        result[0] = -999999;
        result[1] = -999999;
        return;
    }

    // Solve equation
    result[0] = (-beta + sqrt(radical)) / (2 * alpha);
    result[1] = (-beta - sqrt(radical)) / (2 * alpha);
    // sort in ascending order
    std::sort(std::begin(result), std::end(result));
    return;
}

PositionVector3D<Cartesian3D<double>> BentPixelDetector::getLocalIntercept(const Track* track) const {
    return globalToLocal(getIntercept(track));
}

// Function to check if a track intercepts with a plane
bool BentPixelDetector::hasIntercept(const Track* track, double pixelTolerance) const {

    // First, get the track intercept in global coordinates with the plane
    PositionVector3D<Cartesian3D<double>> globalIntercept = this->getIntercept(track);

    // Convert to local coordinates
    PositionVector3D<Cartesian3D<double>> localIntercept = globalToLocal(globalIntercept);

    // Get the row and column numbers
    double row = this->getRow(localIntercept);
    double column = this->getColumn(localIntercept);

    // Check if the row and column are outside of the chip
    // Chip reaches from -0.5 to nPixels-0.5
    bool intercept = true;
    if(row < pixelTolerance - 0.5 || row > (this->m_nPixels.Y() - pixelTolerance - 0.5) || column < pixelTolerance - 0.5 ||
       column > (this->m_nPixels.X() - pixelTolerance - 0.5))
        intercept = false;

    return intercept;
}

// Function to check if a track goes through/near a masked pixel
bool BentPixelDetector::hitMasked(const Track* track, int tolerance) const {

    // First, get the track intercept in global coordinates with the plane
    PositionVector3D<Cartesian3D<double>> globalIntercept = this->getIntercept(track);

    // Convert to local coordinates
    PositionVector3D<Cartesian3D<double>> localIntercept = globalToLocal(globalIntercept);

    // Get the row and column numbers
    int row = static_cast<int>(floor(this->getRow(localIntercept) + 0.5));
    int column = static_cast<int>(floor(this->getColumn(localIntercept) + 0.5));

    // Check if the pixels around this pixel are masked
    bool hitmasked = false;
    for(int r = (row - tolerance); r <= (row + tolerance); r++) {
        for(int c = (column - tolerance); c <= (column + tolerance); c++) {
            if(this->masked(c, r))
                hitmasked = true;
        }
    }

    return hitmasked;
}

// Functions to get row and column from local position
double BentPixelDetector::getRow(const PositionVector3D<Cartesian3D<double>> localPosition) const {
    return localPosition.Y() / m_pitch.Y() + static_cast<double>(m_nPixels.Y() - 1) / 2.;
}
double BentPixelDetector::getColumn(const PositionVector3D<Cartesian3D<double>> localPosition) const {
    return localPosition.X() / m_pitch.X() + static_cast<double>(m_nPixels.X() - 1) / 2.;
}

// Function to get local position from row and column
PositionVector3D<Cartesian3D<double>> BentPixelDetector::getLocalPosition(double column, double row) const {

    return PositionVector3D<Cartesian3D<double>>(m_pitch.X() * (column - static_cast<double>(m_nPixels.X() - 1) / 2.),
                                                 m_pitch.Y() * (row - static_cast<double>(m_nPixels.Y() - 1) / 2.),
                                                 0.);
}

// Function to get in-pixel position
ROOT::Math::XYVector BentPixelDetector::inPixel(const double column, const double row) const {
    // a pixel ranges from (col-0.5) to (col+0.5)
    return XYVector(m_pitch.X() * (column - floor(column + 0.5)), m_pitch.Y() * (row - floor(row + 0.5)));
}

ROOT::Math::XYVector BentPixelDetector::inPixel(const PositionVector3D<Cartesian3D<double>> localPosition) const {
    double column = getColumn(localPosition);
    double row = getRow(localPosition);
    return inPixel(column, row);
}

// Check if track position is within ROI:
bool BentPixelDetector::isWithinROI(const Track* track) const {

    // Empty region of interest:
    if(m_roi.empty()) {
        return true;
    }

    // Check that track is within region of interest using winding number algorithm
    auto localIntercept = this->getLocalIntercept(track);
    auto coordinates = std::make_pair(this->getColumn(localIntercept), this->getRow(localIntercept));
    if(winding_number(coordinates, m_roi) != 0) {
        return true;
    }

    // Outside ROI:
    return false;
}

// Check if cluster is within ROI and/or touches ROI border:
bool BentPixelDetector::isWithinROI(Cluster* cluster) const {

    // Empty region of interest:
    if(m_roi.empty()) {
        return true;
    }

    // Loop over all pixels of the cluster
    for(auto& pixel : cluster->pixels()) {
        if(winding_number(pixel->coordinates(), m_roi) == 0) {
            return false;
        }
    }
    return true;
}

XYVector BentPixelDetector::getSize() const {
    return XYVector(m_pitch.X() * m_nPixels.X(), m_pitch.Y() * m_nPixels.Y());
}

/* Winding number test for a point in a polygon
 * via: http://geomalgorithms.com/a03-_inclusion.html
 *      Input:   x, y = a point,
 *               polygon = vector of vertex points of a polygon V[n+1] with V[n]=V[0]
 *      Return:  wn = the winding number (=0 only when P is outside)
 */
int BentPixelDetector::winding_number(std::pair<int, int> probe, std::vector<std::vector<int>> polygon) {
    // Two points don't make an area
    if(polygon.size() < 3) {
        LOG(DEBUG) << "No ROI given.";
        return 0;
    }

    int wn = 0; // the  winding number counter

    // loop through all edges of the polygon

    // edge from V[i] to  V[i+1]
    for(size_t i = 0; i < polygon.size(); i++) {
        auto point_this = std::make_pair(polygon.at(i).at(0), polygon.at(i).at(1));
        auto point_next = (i + 1 < polygon.size() ? std::make_pair(polygon.at(i + 1).at(0), polygon.at(i + 1).at(1))
                                                  : std::make_pair(polygon.at(0).at(0), polygon.at(0).at(1)));

        // start y <= P.y
        if(point_this.second <= probe.second) {
            // an upward crossing
            if(point_next.second > probe.second) {
                // P left of  edge
                if(isLeft(point_this, point_next, probe) > 0) {
                    // have  a valid up intersect
                    ++wn;
                }
            }
        } else {
            // start y > P.y (no test needed)

            // a downward crossing
            if(point_next.second <= probe.second) {
                // P right of  edge
                if(isLeft(point_this, point_next, probe) < 0) {
                    // have  a valid down intersect
                    --wn;
                }
            }
        }
    }
    return wn;
}
/* isLeft(): tests if a point is Left|On|Right of an infinite line.
 * via: http://geomalgorithms.com/a03-_inclusion.html
 *    Input:  three points P0, P1, and P2
 *    Return: >0 for P2 left of the line through P0 and P1
 *            =0 for P2  on the line
 *            <0 for P2  right of the line
 *    See: Algorithm 1 "Area of Triangles and Polygons"
 */
int BentPixelDetector::isLeft(std::pair<int, int> pt0, std::pair<int, int> pt1, std::pair<int, int> pt2) {
    return ((pt1.first - pt0.first) * (pt2.second - pt0.second) - (pt2.first - pt0.first) * (pt1.second - pt0.second));
}

// Check if a pixel touches any of the pixels in a cluster
bool BentPixelDetector::isNeighbor(const std::shared_ptr<Pixel>& neighbor,
                                   const std::shared_ptr<Cluster>& cluster,
                                   const int neighbor_radius_row,
                                   const int neighbor_radius_col) {
    for(auto pixel : cluster->pixels()) {
        int row_distance = abs(pixel->row() - neighbor->row());
        int col_distance = abs(pixel->column() - neighbor->column());

        if(row_distance <= neighbor_radius_row && col_distance <= neighbor_radius_col) {
            if(row_distance > 1 || col_distance > 1) {
                cluster->setSplit(true);
            }
            return true;
        }
    }
    return false;
}
